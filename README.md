**T40 Datadeck Download Tool**

The T40 Datadeck Data Download Tool from TTM Consulting Pty Ltd (https://www.ttmgroup.com.au) is software which has only been available for Windows. This project replicates operation of that software enabling it to run on Linux systems.

Usage of the Linux version is more or less identical to the Windows version, one visual difference being display of a progress spinner when data flow from the datadeck is detected.


<div align="center">
<img src="images/datadeck-running.png"  width="375" height="226">
</div>

**Requirements**

- datadeck uses the GTK+3 gui framework
- standard development libraries & tools are required
```
    sudo apt-get install build-essential libgtk-3-dev
```

**Building the datadeck executable**

- from the "Releases" page, download and unpack the source code.
- enter the datadeck-[version] directory.
- type: ```make```
- run: ```./datadeck```

**System Installation**

- from the datadeck-[version] directory, run (as root): ```make install```

After a system installation, the _datadeck_ tool can be run via the usual system-wide menus e.g. ```Applications->System->Datadeck```

**Uploading data to Google Drive**

Having saved the datadeck data somewhere locally, it can be uploaded to TTM's data storage area using a web browser (assuming it has been shared with you and is accessible via a "Shared with me" entry in your Google Drive page).

A neater solution is to make your Google Drive (hence the "Shared with me" entry) available in your file manager. This can take some initial setting up but after that allows saving the datadeck data directly to TTM's Google Drive. There are several mechanisms to enable mounting your Google Drive so that is appears in your file manager. I use [_rclone_](https://github.com/rclone/rclone/) - it supports a number of other cloud storage systems beside Google Drive. It can be installed from [_rclone_'s Download page](https://rclone.org/downloads/), after which you'll have to [configure _rclone_](https://rclone.org/docs/) to access your Google Drive. This includes obtaining credentials to allow API access to your Google Drive. It's a bit messy but the instructions are pretty good. After configuration, mount your Google Drive locally with a command something like:

`       rclone mount --daemon --drive-shared-with-me GoogleDrive:/ /home/chris/myGoogleDrive/`

where _GoogleDrive:/_ refers to the name given during configuration of the remote site (in my case it was _GoogleDrive_), and _/home/chris/myGoogleDrive/_ is the name of an empty directory I created - where the Google Drive "Shared with me" data will appear. Now, when running the `datadeck` tool, the `Save File` popup will show a `myGoogleDrive` directory (or whatever the local mount point was named). From there, navigate to the desired storage and save the data.
