#include <errno.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <string.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#if defined(__APPLE__) || defined(__FreeBSD__)
#include <copyfile.h>
#else
#include <sys/sendfile.h>
#endif

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= CLOCAL | CREAD;
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    tty.c_lflag |= ICANON | ISIG;  /* canonical input */
    tty.c_lflag &= ~(ECHO | ECHOE | ECHONL | IEXTEN);

    tty.c_iflag &= ~IGNCR;  /* preserve carriage return */
    tty.c_iflag &= ~INPCK;
    tty.c_iflag &= ~(INLCR | ICRNL | IUCLC | IMAXBEL);
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);   /* no SW flowcontrol */

    tty.c_oflag &= ~OPOST;

    tty.c_cc[VEOL] = 0;
    tty.c_cc[VEOL2] = 0;
    tty.c_cc[VEOF] = 0x04;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}


int read_serial_port(FILE *f_temp_fd, char *portname)
{
    int fd;
    fd_set rfds;
    struct timeval tv;
    int retval;

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    /*baudrate 19200, 8 bits, no parity, 1 stop bit */
    set_interface_attribs(fd, B19200);

    tcdrain(fd);    /* delay for output */


    /* simple canonical input */
    do {
        unsigned char buf[83];
        unsigned char *p;
        int rdlen;
        int bytes_available;
        int maxfd;

        rdlen = read(fd, buf, sizeof(buf) - 1);
        if (rdlen > 0) {
            buf[rdlen] = 0;
            //printf("Read %d: ", rdlen);
            /* first display as hex numbers then ASCII */
            for (p = buf; rdlen-- > 0; p++) {
            //    //printf("0x%x", *p);
                if (*p == '\0') {
                    printf("EOF!\n");
                }
                //if (*p < ' ') {
                //    printf(" 0x%x", *p);
                //    *p = '.';   /* replace any control chars */
                //}
            }
            printf("%s", buf);
            fprintf(f_temp_fd, "%s", buf);
        } else if (rdlen < 0) {
            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
        } else {  /* rdlen == 0 */
            printf("Nothing read. EOF?\n");
        }               

        /* Check whether there's more to read within some timeout, otherwise assume we're done */
        maxfd = fd + 1;
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);

        /* Wait for up to one seconds */
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        retval = select(maxfd, &rfds, NULL, NULL, &tv);
        if (retval == -1 )
            perror("select()");
        else if (retval)
            /* Data should be available (check first with FD_ISSET ?) */
            continue;
        else
            /* No data within timeout period - assume we're done */
            fprintf(f_temp_fd, "\r");
            break;

    } while (1);

    return 0;
}

int OSCopyFile(const char* source, const char* destination)
{    
    int input, output;    
    if ((input = open(source, O_RDONLY)) == -1)
    {
        return -1;
    }    
    if ((output = creat(destination, 0660)) == -1)
    {
        close(input);
        return -1;
    }

    //Here we use kernel-space copying for performance reasons
#if defined(__APPLE__) || defined(__FreeBSD__)
    //fcopyfile works on FreeBSD and OS X 10.5+ 
    int result = fcopyfile(input, output, 0, COPYFILE_ALL);
#else
    //sendfile will work with non-socket output (i.e. regular file) on Linux 2.6.33+
    off_t bytesCopied = 1; /* Don't copy unwanted \r at beginning of file */
    struct stat fileinfo = {0};
    fstat(input, &fileinfo);
    int result = sendfile(output, input, &bytesCopied, fileinfo.st_size);
#endif

    close(input);
    close(output);

    return result;
}


/*
    Create a temporary file.
    Determine which serial port to use.
    Receive serial data into temporary file.
    If data received OK, determine a permanent file location.
    Move temporary file data to permanent file location
*/
int main()
{
    int temp_fd;
    FILE *f_temp_fd;
    int retval;
    char template[32];

/*
    Find which serial port rto use. For now, use /dev/ttyUSB0
*/
    char *port = "/dev/ttyUSB0";

/*
    Create a temp file to write data
*/
    strncpy(template, "/tmp/T40DataDeck_XXXXXX", sizeof template);
    if ( !(temp_fd = mkstemp(template)) )
    {
	perror("mkstemp()");
        return -2;
    }
    printf("Temp file location: %s\n", template); 
    if ( !(f_temp_fd=fdopen(temp_fd, "w+")) )
    {
	perror("fdopen()");
        return -3;
        
    }

    retval = read_serial_port(f_temp_fd, port);
    if (retval != 0)
    {
        printf("ERROR: data not received. Exiting now ...\n");
        return -4;
    }
    fclose(f_temp_fd);

/*
    Find permanent location - for testing, let's use "/home/chris/src/datadeck/T40DataDeck.csv"
*/
    if ( (retval=OSCopyFile(template, "/home/chris/src/datadeck/T40DataDeck.csv")) < 0 )
    {
        perror("OSCopyFile()");
        return -5;
    }
    printf("All done\n");
}
